package net.pl3x.bukkit.shop.task;

import net.pl3x.bukkit.shop.Shop;
import org.bukkit.block.Sign;
import org.bukkit.scheduler.BukkitRunnable;

public class UpdateSign extends BukkitRunnable {
    private Shop shop;

    public UpdateSign(Shop shop) {
        this.shop = shop;
    }

    @Override
    public void run() {
        Sign sign = shop.getSign();
        String[] lines = shop.getLines();

        for (int i = 0; i < 4; i++) {
            sign.setLine(i, lines[i]);
        }

        sign.update();
    }
}
