package net.pl3x.bukkit.shop;

import net.pl3x.bukkit.shop.manager.ShopManager;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;

public class Shop {
    private final Sign sign;
    private final Block block;
    private String[] lines;

    private CachedPlayer owner;
    private int quantity;
    private double buyPrice = -1.0D;
    private double sellPrice = -1.0D;
    private Item item;
    private ShopType type;

    public Shop(Block block, String[] lines) throws IllegalStateException {
        if (!(block.getState() instanceof Sign)) {
            throw new IllegalStateException("Not a sign block");
        }

        if (lines[2].indexOf(':') != lines[2].lastIndexOf(':')) {
            throw new IllegalStateException("Price line contains too many colons");
        }

        this.lines = lines;
        this.block = block;
        this.sign = (Sign) block.getState();
    }

    public Sign getSign() {
        return sign;
    }

    public String[] getLines() {
        return lines;
    }

    public void setLines(String[] lines) {
        this.lines = lines;
    }

    public String getLine(int line) {
        return lines[line];
    }

    public void setLine(int line, String text) {
        lines[line] = text;
    }

    public boolean isAdminShop() {
        return getType().equals(ShopType.ADMIN);
    }

    public Chest getChest() {
        for (BlockFace face : ShopManager.SHOP_FACES) {
            BlockState relative = block.getRelative(face).getState();
            if (relative instanceof Chest) {
                return (Chest) relative;
            }
        }
        return null;
    }

    public CachedPlayer getOwner() {
        return owner;
    }

    public void setOwner(CachedPlayer owner) {
        this.owner = owner;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public ShopType getType() {
        return type;
    }

    public void setType(ShopType type) {
        this.type = type;
    }

    public enum ShopType {
        ADMIN,
        PLAYER
    }
}
