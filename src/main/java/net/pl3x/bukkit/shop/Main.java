package net.pl3x.bukkit.shop;

import net.pl3x.bukkit.shop.command.CmdPl3xShop;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.hook.Vault;
import net.pl3x.bukkit.shop.listener.ShopListener;
import net.pl3x.bukkit.shop.listener.postshopcreation.ChargeFee;
import net.pl3x.bukkit.shop.listener.postshopcreation.Confirmation;
import net.pl3x.bukkit.shop.listener.postshopcreation.SetLines;
import net.pl3x.bukkit.shop.listener.preshopcreation.ChestChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.ErrorChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.ItemChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.MoneyChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.NameChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.PermissionChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.PriceChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.PriceRatioChecker;
import net.pl3x.bukkit.shop.listener.preshopcreation.QuantityChecker;
import net.pl3x.bukkit.shop.manager.ShopManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        PluginManager pm = Bukkit.getPluginManager();

        if (failedDependencyCheck()) {
            Logger.error("Fix the above issues and restart the server to enable this plugin.");
            pm.disablePlugin(this);
            return;
        }

        ShopManager.loadAllShops();

        pm.registerEvents(new ShopListener(), this);

        pm.registerEvents(new ChestChecker(), this);
        pm.registerEvents(new ErrorChecker(), this);
        pm.registerEvents(new ItemChecker(), this);
        pm.registerEvents(new MoneyChecker(), this);
        pm.registerEvents(new NameChecker(), this);
        pm.registerEvents(new PermissionChecker(), this);
        pm.registerEvents(new PriceChecker(), this);
        pm.registerEvents(new PriceRatioChecker(), this);
        pm.registerEvents(new QuantityChecker(), this);

        pm.registerEvents(new ChargeFee(), this);
        pm.registerEvents(new Confirmation(), this);
        pm.registerEvents(new SetLines(), this);

        getCommand("shop").setExecutor(new CmdPl3xShop(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        ShopManager.unloadAllShops();

        Logger.info(getName() + " Disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.DARK_RED + getName() + " is disabled. Please check console logs for more information.");
        return true;
    }

    /**
     * See if all dependencies are installed and enabled
     *
     * @return True is a dependency is not found/enabled
     */
    private boolean failedDependencyCheck() {
        if (!Bukkit.getPluginManager().isPluginEnabled("Vault")) {
            Logger.error("Vault plugin is required to be installed!");
            return true;
        }
        if (Vault.setupEconomyFailed()) {
            Logger.error("Economy plugin is required to be installed!");
            return true;
        }
        return false;
    }
}
