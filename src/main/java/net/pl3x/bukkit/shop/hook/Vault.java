package net.pl3x.bukkit.shop.hook;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.bukkit.shop.configuration.Config;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.exception.VaultException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.text.DecimalFormat;
import java.util.UUID;

public class Vault {
    private static Economy economy = null;

    public static Economy getEconomy() {
        return economy;
    }

    public static boolean setupEconomyFailed() {
        RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return economy == null;
    }

    public static double getBalance(Player player) {
        return getBalance(player.getUniqueId());
    }

    public static double getBalance(UUID uuid) {
        return economy.getBalance(Bukkit.getOfflinePlayer(uuid));
    }

    public static String format(double amount) {
        DecimalFormat format = new DecimalFormat(Config.CURRENCY_FORMAT.getString());
        return format.format(amount);
    }

    public static void withdrawPlayer(UUID uuid, double amount) throws VaultException {
        EconomyResponse response = getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(uuid), amount);
        if (!response.transactionSuccess()) {
            throw new VaultException(Lang.VAULT_WITHDRAW_ERROR.replace("{error}", response.errorMessage));
        }
    }

    public static void depositPlayer(UUID uuid, double amount) throws VaultException {
        EconomyResponse response = getEconomy().depositPlayer(Bukkit.getOfflinePlayer(uuid), amount);
        if (!response.transactionSuccess()) {
            throw new VaultException(Lang.VAULT_DEPOSIT_ERROR.replace("{error}", response.errorMessage));
        }
    }
}
