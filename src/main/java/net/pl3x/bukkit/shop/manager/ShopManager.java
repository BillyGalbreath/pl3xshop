package net.pl3x.bukkit.shop.manager;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.configuration.AdminShopConfig;
import net.pl3x.bukkit.shop.configuration.PlayerShopConfig;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;

import java.util.HashMap;
import java.util.Map;

public class ShopManager {
    /*
     * The directions a chest can be from the shop sign
     */
    public static final BlockFace[] SHOP_FACES = {
            BlockFace.DOWN,
            BlockFace.UP,
            BlockFace.NORTH,
            BlockFace.SOUTH,
            BlockFace.EAST,
            BlockFace.WEST,
    };

    private static final Map<Location, Shop> shops = new HashMap<>();

    /**
     * Get a shop loaded in memory
     *
     * @param location Location of shop
     * @return Shop at location
     */
    public static Shop getShop(Location location) {
        return shops.get(location);
    }

    /**
     * Add a new shop to memory and config
     *
     * @param shop Shop to create/modify
     */
    public static void setShop(Shop shop) {
        switch (shop.getType()) {
            case ADMIN:
                AdminShopConfig.getConfig().setShop(shop);
                break;
            case PLAYER:
                PlayerShopConfig.getConfig(shop.getOwner()).setShop(shop);
                break;
            default:
                throw new IllegalStateException("Cannot determine shop type!");
        }
        shops.put(shop.getSign().getLocation(), shop);
    }

    /**
     * Remove a shop from memory and config
     *
     * @param location Location of sign
     */
    public static void removeShop(Location location) {
        unloadShop(location);

        // TODO remove from config
    }

    public static void unloadShop(Location location) {
        shops.remove(location);
    }

    /**
     * Load all shops from config files (unloads existing shops first)
     */
    public static void loadAllShops() {
        unloadAllShops();

        AdminShopConfig.getConfig().loadAllShops();
        PlayerShopConfig.loadAllConfigs();
    }

    /**
     * Unload all shops from memory
     */
    public static void unloadAllShops() {
        shops.clear();

        AdminShopConfig.unloadConfig();
        PlayerShopConfig.unloadAllConfigs();
    }
}
