package net.pl3x.bukkit.shop.exception;

public class VaultException extends RuntimeException {
    public VaultException(String str) {
        super(str);
    }
}
