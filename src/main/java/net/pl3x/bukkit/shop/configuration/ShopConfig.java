package net.pl3x.bukkit.shop.configuration;

import net.pl3x.bukkit.shop.Item;
import net.pl3x.bukkit.shop.Main;
import net.pl3x.bukkit.shop.Shop;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public abstract class ShopConfig extends YamlConfiguration {
    private final File file;
    private final Object saveLock = new Object();

    public ShopConfig(String fileName) {
        super();
        file = new File(Main.getPlugin(Main.class).getDataFolder(), fileName);
        load();
    }

    private void load() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }

    protected void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public void setShop(Shop shop) {
        String path = locationToString(shop.getSign().getLocation());

        set(path + ".quantity", shop.getQuantity());
        set(path + ".buy", shop.getBuyPrice());
        set(path + ".sell", shop.getSellPrice());
        set(path + ".item", shop.getItem().name());

        save();
    }

    protected Location stringToLocation(String path) {
        String[] split = path.split(",");

        World world = Bukkit.getWorld(split[0]);
        if (world == null) {
            return null;
        }

        try {
            int x = Integer.valueOf(split[1]);
            int y = Integer.valueOf(split[2]);
            int z = Integer.valueOf(split[3]);
            return new Location(world, x, y, z);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private String locationToString(Location location) {
        return location.getWorld().getName() + "," +
                location.getBlockX() + "," +
                location.getBlockY() + "," +
                location.getBlockZ();
    }

    protected Item getItem(String path) {
        String itemName = getString(path).toUpperCase();
        for (Item item : Item.values()) {
            if (item.name().equals(itemName)) {
                return item;
            }
        }
        return null;
    }
}
