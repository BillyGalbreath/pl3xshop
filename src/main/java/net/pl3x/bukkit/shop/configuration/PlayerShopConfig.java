package net.pl3x.bukkit.shop.configuration;

import net.pl3x.bukkit.shop.Logger;
import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.manager.ShopManager;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerShopConfig extends ShopConfig {
    private static final Map<UUID, PlayerShopConfig> configs = new HashMap<>();

    public static PlayerShopConfig getConfig(CachedPlayer player) {
        return getConfig(player.getUniqueId());
    }

    private static PlayerShopConfig getConfig(UUID uuid) {
        synchronized (configs) {
            if (configs.containsKey(uuid)) {
                return configs.get(uuid);
            }
            PlayerShopConfig config = new PlayerShopConfig(uuid);
            configs.put(uuid, config);
            return config;
        }
    }

    public static void loadAllConfigs() {
        // TODO
    }

    public static void unloadAllConfigs() {
        Collection<PlayerShopConfig> oldConfs = new ArrayList<>(configs.values());
        synchronized (configs) {
            oldConfs.forEach(PlayerShopConfig::discard);
        }
    }

    private UUID uuid;

    public PlayerShopConfig(UUID uuid) {
        super("player-shops" + File.separator + uuid + ".yml");
        this.uuid = uuid;
    }

    public void discard() {
        save();
        configs.remove(uuid);
    }

    public void setShop(Shop shop) {
        // TODO
    }

    public void loadAllShops() {
        for (String path : getConfigurationSection("shops").getValues(false).keySet()) {
            Location location = stringToLocation(path);
            if (location == null) {
                Logger.warn("Player Shop cannot be loaded. Invalid location. (" + uuid + ": " + path + ")");
                continue;
            }

            Block block = location.getBlock();
            Shop shop;
            try {
                shop = new Shop(block, ((Sign) block).getLines());
            } catch (IllegalStateException e) {
                Logger.warn("Player Shop cannot be loaded. (" + uuid + ": " + path + ")");
                Logger.warn("   " + e.getMessage());
                continue;
            }

            shop.setQuantity(getInt(path + ".quantity", 0));
            shop.setBuyPrice(getDouble(path + ".buy", -1.0D));
            shop.setSellPrice(getDouble(path + ".sell", -1.0D));
            shop.setItem(getItem(path + ".item"));
            shop.setType(Shop.ShopType.ADMIN);

            ShopManager.setShop(shop);
        }
    }
}
