package net.pl3x.bukkit.shop.configuration;

import net.pl3x.bukkit.shop.Main;

/**
 * The main config.yml manager
 */
public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    ADMIN_SHOP_NAME("Admin Shop"),
    SHOP_CREATION_PRICE(0.0D),
    SHOP_MODIFICATION_PRICE(0.0D),
    CURRENCY_FORMAT("$0.00");

    private final Main plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
    }

    /**
     * Reload config from disk to memory
     */
    public static void reload() {
        Main.getPlugin(Main.class).reloadConfig();
    }

    /**
     * Get entry key
     *
     * @return Entry key
     */
    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Get string value
     *
     * @return String value
     */
    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    /**
     * Get boolean value
     *
     * @return Boolean value
     */
    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    /**
     * Get double value
     *
     * @return Double value
     */
    public double getDouble() {
        return plugin.getConfig().getDouble(getKey(), (Double) def);
    }
}
