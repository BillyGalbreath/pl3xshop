package net.pl3x.bukkit.shop.configuration;

import net.pl3x.bukkit.shop.Logger;
import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.manager.ShopManager;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;

public class AdminShopConfig extends ShopConfig {
    private static AdminShopConfig config;

    public static AdminShopConfig getConfig() {
        if (config == null) {
            config = new AdminShopConfig();
        }
        return config;
    }

    public static void unloadConfig() {
        getConfig().save();
        config = null;
    }

    public AdminShopConfig() {
        super("shops.yml");
    }

    public void loadAllShops() {
        ConfigurationSection cs = getConfigurationSection("shops");
        if (cs == null) {
            return; // empty config
        }
        for (String path : cs.getValues(false).keySet()) {
            Location location = stringToLocation(path);
            if (location == null) {
                Logger.warn("Admin Shop cannot be loaded. Invalid location. (" + path + ")");
                continue;
            }

            Block block = location.getBlock();
            Shop shop;
            try {
                shop = new Shop(block, ((Sign) block).getLines());
            } catch (IllegalStateException e) {
                Logger.warn("Admin Shop cannot be loaded. (" + path + ")");
                Logger.warn("   " + e.getMessage());
                continue;
            }

            shop.setQuantity(getInt(path + ".quantity", 0));
            shop.setBuyPrice(getDouble(path + ".buy", -1.0D));
            shop.setSellPrice(getDouble(path + ".sell", -1.0D));
            shop.setItem(getItem(path + ".item"));
            shop.setType(Shop.ShopType.ADMIN);

            ShopManager.setShop(shop);
        }
    }
}
