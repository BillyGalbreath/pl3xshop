package net.pl3x.bukkit.shop.configuration;

import net.pl3x.bukkit.shop.Logger;
import net.pl3x.bukkit.shop.Main;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Language entries for regional/customized message support
 */
public enum Lang {
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!"),

    VAULT_WITHDRAW_ERROR("&4An error has occurred trying to withdraw from account! {error}"),
    VAULT_DEPOSIT_ERROR("&4An error has occurred trying to deposit to account! {error}"),

    SIGN_NAME_FORMAT("&1[&3{name}&1]"),

    INVALID_ITEM("&4Invalid item specified!"),
    INVALID_PRICE("&4 Invalid price specified!"),
    INVALID_QUANTITY("&4Invalid quantity specified!"),
    NO_CHEST("&4No chest detected!"),
    NO_CHEST_PERMISSION("&4You cannot create a shop here!"),
    NO_ADMIN_PERMISSION("&4You do not have permission to do that!"),
    NO_CREATE_PERMISSION("&4You do not have permission to create that shop!"),
    NO_MODIFY_PERMISSION("&4You do not have permission to modify that shop!"),
    NOT_ENOUGH_FUNDS("&4You do not have enough money!"),
    SELL_HIGHER_THAN_BUY("&4Sell price cannot be higher than buy price!"),

    SHOP_CREATED("&dShop created."),
    SHOP_MODIFIED("&dShop modified."),
    SHOP_REMOVED("&dShop removed."),
    SHOP_FEE_CHARGED("&7{cost} &dhas been deducted from your account."),

    VERSION("&d{plugin} v{version}"),
    RELOAD("&d{plugin} v{version} reloaded.");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    /**
     * Reload lang file if not already loaded into memory
     */
    public static void reload() {
        reload(false);
    }

    /**
     * Reload lang file
     *
     * @param force True to force memory overwrite from disk
     */
    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Main.getPlugin(Main.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Main.getPlugin(Main.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    /**
     * Get the key for this entry
     *
     * @return Entry key
     */
    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Get String entry from memory
     *
     * @return String entry
     */
    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    /**
     * Replace String with a String
     *
     * @param find    String to find
     * @param replace String to take its place
     * @return String result
     */
    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
