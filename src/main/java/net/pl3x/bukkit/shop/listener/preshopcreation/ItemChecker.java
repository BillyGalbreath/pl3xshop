package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Item;
import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class ItemChecker implements Listener {
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();

        String code = shop.getLine(3);

        Item item = Item.getItem(code);

        if (item == null) {
            event.setResult(PreShopCreationEvent.Result.INVALID_ITEM);
            return;
        }

        shop.setItem(item);
        shop.setLine(3, item.getDisplayName());
    }
}
