package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class PermissionChecker implements Listener {
    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();

        Player player = event.getPlayer();

        if (!player.hasPermission("shop.create")) {
            event.setResult(PreShopCreationEvent.Result.NO_CREATE_PERMISSION);
            return;
        }

        if (event.isModifying() && !player.hasPermission("shop.modify")) {
            event.setResult(PreShopCreationEvent.Result.NO_MODIFY_PERMISSION);
            return;
        }

        if (shop.isAdminShop() && !player.hasPermission("shop.admin")) {
            event.setResult(PreShopCreationEvent.Result.NO_ADMIN_PERMISSION);
        }
    }
}
