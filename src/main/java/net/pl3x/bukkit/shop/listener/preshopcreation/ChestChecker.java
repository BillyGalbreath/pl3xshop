package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import org.bukkit.Bukkit;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ChestChecker implements Listener {
    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();

        if (shop.isAdminShop()) {
            return; // admin shop doesnt need chest
        }

        Chest chest = shop.getChest();
        if (chest == null) {
            event.setResult(PreShopCreationEvent.Result.NO_CHEST);
            return;
        }

        BlockBreakEvent blockBreakEvent = new BlockBreakEvent(chest.getBlock(), event.getPlayer());
        Bukkit.getPluginManager().callEvent(blockBreakEvent);
        if (blockBreakEvent.isCancelled()) {
            event.setResult(PreShopCreationEvent.Result.NO_CHEST_PERMISSION);
        }
    }
}
