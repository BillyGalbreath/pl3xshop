package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.configuration.Config;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import net.pl3x.bukkit.shop.hook.Vault;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class MoneyChecker implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        double cost = event.isModifying() ? Config.SHOP_MODIFICATION_PRICE.getDouble() : Config.SHOP_CREATION_PRICE.getDouble();

        if (cost <= 0.0D) {
            return;
        }

        Shop shop = event.getShop();

        if (shop.isAdminShop()) {
            return; // admin shops are free
        }

        if (shop.getBuyPrice() == 0.0D && shop.getSellPrice() == 0.0D) {
            return; // free shops are free
        }

        Player player = event.getPlayer();

        if (player.hasPermission("shop.nofee")) {
            return; // player doesnt get charged fee
        }

        double balance = Vault.getBalance(player);

        if (cost > balance) {
            event.setResult(PreShopCreationEvent.Result.NOT_ENOUGH_FUNDS);
        }
    }
}
