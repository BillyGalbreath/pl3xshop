package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.configuration.Config;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import net.pl3x.bukkit.usercache.api.UserCache;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class NameChecker implements Listener {
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();
        Player player = event.getPlayer();

        String firstLine = shop.getLine(0).toLowerCase().replace(" ", "");
        String name = firstLine.substring(1, firstLine.length() - 1); // chop off [] chars and remove spaces

        Shop.ShopType type;
        CachedPlayer owner = null;
        String adminShopName = Config.ADMIN_SHOP_NAME.getString();
        String newName;

        if (name.equalsIgnoreCase("shop")) {
            type = Shop.ShopType.PLAYER;
            owner = UserCache.getCachedPlayer(player.getName());
            if (owner == null) {
                event.setResult(PreShopCreationEvent.Result.INVALID_NAME);
                return;
            }
            newName = owner.getName();
        } else if (name.equalsIgnoreCase("adminshop") || name.equalsIgnoreCase(adminShopName.toLowerCase().replace(" ", ""))) {
            type = Shop.ShopType.ADMIN;
            newName = adminShopName;
        } else {
            event.setResult(PreShopCreationEvent.Result.INVALID_NAME);
            return;
        }

        if (newName.length() > 14) {
            newName = newName.substring(0, 14);
        }

        shop.setLine(0, Lang.SIGN_NAME_FORMAT.replace("{name}", newName));
        shop.setOwner(owner);
        shop.setType(type);
    }
}
