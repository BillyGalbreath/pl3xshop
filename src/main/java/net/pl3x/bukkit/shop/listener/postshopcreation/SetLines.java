package net.pl3x.bukkit.shop.listener.postshopcreation;

import net.pl3x.bukkit.shop.Main;
import net.pl3x.bukkit.shop.event.ShopCreationEvent;
import net.pl3x.bukkit.shop.task.UpdateSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SetLines implements Listener {
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onShopCreation(ShopCreationEvent event) {
        new UpdateSign(event.getShop()).runTaskLater(Main.getPlugin(Main.class), 5);
    }
}
