package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import net.pl3x.bukkit.shop.hook.Vault;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class PriceChecker implements Listener {
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();

        String line = shop.getLine(2).toUpperCase().replace("$", "");

        if (line.indexOf('B') != line.lastIndexOf('B') || line.indexOf('S') != line.lastIndexOf('S')) {
            event.setResult(PreShopCreationEvent.Result.INVALID_PRICE);
            return;
        }

        String[] parts;

        if (line.contains(":")) {
            parts = line.split(":");
        } else {
            parts = new String[]{line};
        }

        for (String part : parts) {
            if (part.contains("B")) {
                shop.setBuyPrice(getPrice(part, "B"));
            } else if (part.contains("S")) {
                shop.setSellPrice(getPrice(part, "S"));
            } else {
                shop.setBuyPrice(getPrice("B" + part, "B"));
            }
        }

        double buyPrice = shop.getBuyPrice();
        double sellPrice = shop.getSellPrice();

        if (buyPrice < 0.0D && sellPrice < 0.0D) {
            event.setResult(PreShopCreationEvent.Result.INVALID_PRICE);
            return;
        }

        String newLine = "";

        if (buyPrice >= 0.0D) {
            newLine = "B " + (buyPrice == 0.0D ? "free" : Vault.format(buyPrice));
        }

        if (sellPrice >= 0.0D) {
            if (!newLine.isEmpty()) {
                newLine += " : ";
            }
            newLine += (sellPrice == 0.0D ? "free" : Vault.format(sellPrice)) + " S";
        }

        if (newLine.length() > 15) {
            newLine = newLine.replace(" ", "");
        }

        if (newLine.length() > 15) {
            event.setResult(PreShopCreationEvent.Result.INVALID_PRICE);
            return;
        }

        shop.setLine(2, newLine);
    }

    private double getPrice(String text, String type) {
        if (!text.startsWith(type) && !text.endsWith(type)) {
            return -1.0D;
        }
        text = text.replace(type, "").toLowerCase().trim();
        if (text.equals("free")) {
            return 0.0D;
        }
        try {
            double price = Double.valueOf(text);
            if (Double.isInfinite(price) || price < 0.0D) {
                return -1.0D;
            }
            return price;
        } catch (NumberFormatException e) {
            return -1.0D;
        }
    }
}
