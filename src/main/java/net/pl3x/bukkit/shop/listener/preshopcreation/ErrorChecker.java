package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Chat;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class ErrorChecker implements Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPreShopCreationFailed(PreShopCreationEvent event) {
        if (!event.isCancelled()) {
            return; // no error
        }

        Lang error = null;

        switch (event.getResult()) {
            case INVALID_NAME:
                return; // DO NOT ERROR OR BREAK SIGN! (Could be a mechanical sign for another plugin)
            case INVALID_ITEM:
                error = Lang.INVALID_ITEM;
                break;
            case INVALID_PRICE:
                error = Lang.INVALID_PRICE;
                break;
            case INVALID_QUANTITY:
                error = Lang.INVALID_QUANTITY;
                break;
            case NO_CHEST:
                error = Lang.NO_CHEST;
                break;
            case NO_CHEST_PERMISSION:
                error = Lang.NO_CHEST_PERMISSION;
                break;
            case NO_ADMIN_PERMISSION:
                error = Lang.NO_ADMIN_PERMISSION;
                break;
            case NO_CREATE_PERMISSION:
                error = Lang.NO_CREATE_PERMISSION;
                break;
            case NO_MODIFY_PERMISSION:
                error = Lang.NO_MODIFY_PERMISSION;
                break;
            case NOT_ENOUGH_FUNDS:
                error = Lang.NOT_ENOUGH_FUNDS;
                break;
            case SELL_HIGHER_THAN_BUY:
                error = Lang.SELL_HIGHER_THAN_BUY;
                break;
            default:
        }

        if (error == null) {
            return;
        }

        new Chat(error).send(event.getPlayer());
        event.getShop().getSign().getBlock().breakNaturally();
    }
}
