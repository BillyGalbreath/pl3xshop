package net.pl3x.bukkit.shop.listener.postshopcreation;

import net.pl3x.bukkit.shop.Chat;
import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.configuration.Config;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.event.ShopCreationEvent;
import net.pl3x.bukkit.shop.exception.VaultException;
import net.pl3x.bukkit.shop.hook.Vault;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class ChargeFee implements Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onShopCreation(ShopCreationEvent event) {
        double cost = event.isModifying() ? Config.SHOP_MODIFICATION_PRICE.getDouble() : Config.SHOP_CREATION_PRICE.getDouble();

        if (cost <= 0.0D) {
            return;
        }

        Shop shop = event.getShop();

        if (shop.isAdminShop()) {
            return; // admin shops are free
        }

        if (shop.getBuyPrice() == 0.0D && shop.getSellPrice() == 0.0D) {
            return; // free shops are free
        }

        Player player = event.getPlayer();

        if (player.hasPermission("shop.nofee")) {
            return; // player doesnt get charged fee
        }

        try {
            Vault.withdrawPlayer(player.getUniqueId(), cost);
        } catch (VaultException e) {
            new Chat(e.getMessage()).send(player);
            return;
        }

        new Chat(Lang.SHOP_FEE_CHARGED
                .replace("{cost}", Double.toString(cost)))
                .send(player);
    }
}
