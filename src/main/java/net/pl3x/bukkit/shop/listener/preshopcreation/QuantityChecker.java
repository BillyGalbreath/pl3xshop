package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class QuantityChecker implements Listener {
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();

        int quantity;

        try {
            quantity = Integer.parseInt(shop.getLine(1));
        } catch (NumberFormatException e) {
            event.setResult(PreShopCreationEvent.Result.INVALID_QUANTITY);
            return;
        }

        shop.setQuantity(quantity);
    }
}
