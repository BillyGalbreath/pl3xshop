package net.pl3x.bukkit.shop.listener.postshopcreation;

import net.pl3x.bukkit.shop.Chat;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.event.ShopCreationEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class Confirmation implements Listener {
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onShopCreation(ShopCreationEvent event) {
        new Chat(event.isModifying() ? Lang.SHOP_MODIFIED : Lang.SHOP_CREATED).send(event.getPlayer());
    }
}
