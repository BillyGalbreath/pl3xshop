package net.pl3x.bukkit.shop.listener.preshopcreation;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class PriceRatioChecker implements Listener {
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPreShopCreation(PreShopCreationEvent event) {
        if (event.isCancelled()) {
            return; // explicit check
        }

        Shop shop = event.getShop();

        double buyPrice = shop.getBuyPrice();
        double sellPrice = shop.getSellPrice();

        if (buyPrice >= 0.0D && sellPrice >= 0.0D && sellPrice > buyPrice) {
            event.setResult(PreShopCreationEvent.Result.SELL_HIGHER_THAN_BUY);
        }
    }
}
