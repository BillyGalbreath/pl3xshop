package net.pl3x.bukkit.shop.listener;

import net.pl3x.bukkit.shop.Chat;
import net.pl3x.bukkit.shop.Logger;
import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.event.PreShopCreationEvent;
import net.pl3x.bukkit.shop.event.ShopCreationEvent;
import net.pl3x.bukkit.shop.manager.ShopManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class ShopListener implements Listener {
    /**
     * Shop creation/modification
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void onSignEdit(SignChangeEvent event) {
        Player player = event.getPlayer();

        Shop shop;
        try {
            shop = new Shop(event.getBlock(), event.getLines());
        } catch (IllegalStateException e) {
            Logger.debug("Not a shop: " + e.getMessage());
            removeOldShop(event.getBlock().getLocation(), player, true);
            return;
        }

        PreShopCreationEvent preShopCreationEvent = new PreShopCreationEvent(player, shop);
        Bukkit.getPluginManager().callEvent(preShopCreationEvent);
        if (preShopCreationEvent.isCancelled()) {
            removeOldShop(shop.getSign().getLocation(), player, true);
            return;
        }

        ShopManager.setShop(shop);

        ShopCreationEvent postShopCreationEvent = new ShopCreationEvent(player, shop, preShopCreationEvent.isModifying());
        Bukkit.getPluginManager().callEvent(postShopCreationEvent);
    }

    private void removeOldShop(Location location, Player player, boolean pop) {
        Shop oldShop = ShopManager.getShop(location);
        if (oldShop != null) {
            ShopManager.removeShop(location);
            new Chat(Lang.SHOP_REMOVED).send(player);
            if (pop) {
                Block block = location.getBlock();
                if (block.getState() instanceof Sign) {
                    block.breakNaturally();
                }
            }
        }
    }

    /**
     * Shop buy/sell transaction
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onSignClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return; // ignore creative
        }

        if (!event.getAction().equals(Action.LEFT_CLICK_BLOCK) && !event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return; // did not click a block
        }

        Block block = event.getClickedBlock();
        Shop shop = ShopManager.getShop(block.getLocation());

        if (shop == null) {
            return; // block is not a registered shop sign
        }

        if (!(block.getState() instanceof Sign)) {
            Logger.warn("Sign shop detected on a block that is not a sign! Unloading shop." + block.getLocation());
            ShopManager.unloadShop(block.getLocation());
            return; // block is not a sign
        }

        // temp debug message
        event.getPlayer().sendMessage("TODO: " + (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) ? "Right" : "Left") + " Clicked: " + shop.getType().name());
    }

    /**
     * Shop sign break
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSignBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Shop shop = ShopManager.getShop(block.getLocation());

        if (shop == null) {
            return; // block is not a registered shop sign
        }

        if (!(block.getState() instanceof Sign)) {
            Logger.warn("Sign shop detected on a block that is not a sign! Unloading shop." + block.getLocation());
            ShopManager.unloadShop(block.getLocation());
            return; // block is not a sign
        }

        // TODO check for chest breaks (chest checks faces for signs)

        // TODO change this to custom events
        removeOldShop(block.getLocation(), event.getPlayer(), false);
    }
}
