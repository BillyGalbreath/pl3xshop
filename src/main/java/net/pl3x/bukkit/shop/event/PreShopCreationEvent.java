package net.pl3x.bukkit.shop.event;

import net.pl3x.bukkit.shop.Shop;
import net.pl3x.bukkit.shop.manager.ShopManager;
import org.bukkit.entity.Player;

public class PreShopCreationEvent extends PlayerEvent {
    private Shop shop;
    private boolean modifying = false;
    private Result result = Result.SUCCESS;

    public PreShopCreationEvent(Player player, Shop shop) {
        super(player);
        this.shop = shop;

        modifying = ShopManager.getShop(shop.getSign().getLocation()) != null;
    }

    public boolean isModifying() {
        return modifying;
    }

    public boolean isCancelled() {
        return !(result == Result.SUCCESS || result == Result.MODIFIED);
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public enum Result {
        INVALID_NAME,
        INVALID_ITEM,
        INVALID_PRICE,
        INVALID_QUANTITY,
        NO_CHEST,
        NO_CHEST_PERMISSION,
        NO_ADMIN_PERMISSION,
        NO_CREATE_PERMISSION,
        NO_MODIFY_PERMISSION,
        NOT_ENOUGH_FUNDS,
        SELL_HIGHER_THAN_BUY,
        SUCCESS,
        MODIFIED
    }
}
