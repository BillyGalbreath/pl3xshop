package net.pl3x.bukkit.shop.event;

import net.pl3x.bukkit.shop.Shop;
import org.bukkit.entity.Player;

public class ShopCreationEvent extends PlayerEvent {
    private final Shop shop;
    private final boolean modifying;

    public ShopCreationEvent(Player player, Shop shop, boolean modifying) {
        super(player);
        this.shop = shop;
        this.modifying = modifying;
    }

    public Shop getShop() {
        return shop;
    }

    public boolean isModifying() {
        return modifying;
    }
}
