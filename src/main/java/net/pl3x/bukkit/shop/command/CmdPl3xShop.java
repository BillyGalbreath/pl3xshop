package net.pl3x.bukkit.shop.command;

import net.pl3x.bukkit.shop.Chat;
import net.pl3x.bukkit.shop.Main;
import net.pl3x.bukkit.shop.configuration.Config;
import net.pl3x.bukkit.shop.configuration.Lang;
import net.pl3x.bukkit.shop.manager.ShopManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.List;

public class CmdPl3xShop implements TabExecutor {
    private Main plugin;

    public CmdPl3xShop(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        List<String> result = new ArrayList<>();
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            result.add("reload");
        }
        return result;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1 && "reload".equalsIgnoreCase(args[0])) {
            if (!sender.hasPermission("command.shop.reload")) {
                new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
                return true;
            }

            new Chat(Lang.VERSION
                    .replace("{version}", plugin.getDescription().getVersion())
                    .replace("{plugin}", plugin.getName()))
                    .send(sender);
        }

        if (!sender.hasPermission("command.shop")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        Config.reload();
        ShopManager.unloadAllShops();
        Lang.reload(true);
        ShopManager.loadAllShops();

        new Chat(Lang.RELOAD
                .replace("{plugin}", plugin.getName())
                .replace("{version}", plugin.getDescription().getVersion()))
                .send(sender);

        return true;
    }
}
